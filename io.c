/*
 * io.c
 *
 *  Created on: 29 янв. 2018 г.
 *      Author: svetozar
 */

#include "io.h"
#include "base/io.h"
#include "internal/io.h"

#include <SDR/BASE/Multiplex/common.h>

void init_TARGET_IO(TARGET_IO_t * This, TARGET_IO_Cfg_t * Cfg)
{
#if SDR_SAMPLE_SIZE == 32
  SDR_ASSERT(sizeof(Sample_t) == 4);
#endif

  This->cfg = *Cfg;

  This->Parent = 0;
  This->cbk.rx_UserData_ready = 0;

  symMultiplexCfg_t cfgMux;
  cfgMux.Buf.P = This->txMuxBuf+2+2; /* 2 байта для размера, 2 байта для crc16 */
  cfgMux.Buf.Size = SDR_TARGET_IO_MUX_BUFSIZE;
  cfgMux.symSize = 8;
  init_symMultiplex(&This->txMux, &cfgMux);
}

/*--------------------------------------------------------------------------------------*/

static inline Bool_t mux_prepare_for_write(TARGET_IO_t * This, HOST_DataId_t id)
{
  if (!IO_Mux_prepare_for_write(&This->txMux)) return false;
  return sym_multiplex_write_uint8(&This->txMux, id, 8);
}

static inline Bool_t send_from_mux(TARGET_IO_t * This)
{
  if (!IO_Mux_prepare_for_send(&This->txMux)) return false;
  Size_t size = symMultiplex_Count(&This->txMux);
  return TARGET_IO_raw_send_buf(This, symMultiplex_Data(&This->txMux), size) == size;
}

Bool_t TARGET_IO_send_data(TARGET_IO_t * This, TARGET_DataId_t id, UInt8_t * Buf, Size_t Count)
{
  if (!mux_prepare_for_write(This, id)) return false;
  if (!sym_multiplex_write_uint32(&This->txMux, Count, 32)) return false;
  Size_t n = sym_multiplex_write_buf(&This->txMux, Buf, Count);
  if (n != Count) return false;
  return send_from_mux(This);
}

Bool_t TARGET_IO_send_params(TARGET_IO_t * This, TARGET_Params_t * Params)
{
  if (!mux_prepare_for_write(This, HOST_SetParamsId)) return false;
  if (!sym_multiplex_write_uint32(&This->txMux, Params->timeout_ms, 32)) return false;
  return send_from_mux(This);
}

Bool_t TARGET_IO_send_command(TARGET_IO_t *This, HOST_Command_t cmd)
{
  if (!mux_prepare_for_write(This, HOST_CommandId)) return false;
  if (!sym_multiplex_write_uint8(&This->txMux, cmd, 8)) return false;
  return send_from_mux(This);
}

Bool_t TARGET_IO_send_value(TARGET_IO_t *This, TARGET_DataId_t id, const char * value)
{
  if (!mux_prepare_for_write(This, HOST_CommandId)) return false;
  if (!sym_multiplex_write_uint8(&This->txMux, HOST_setValue, 8)) return false;
  if (!sym_multiplex_write_uint8(&This->txMux, id, 8)) return false;
  UInt16_t len = strlen(value);
  if (!sym_multiplex_write_uint16(&This->txMux, len, 16)) return false;
  sym_multiplex_write_str(&This->txMux, value, len);
  return send_from_mux(This);
}

/*--------------------------------------------------------------------------------------*/

static inline TARGET_IO_Result_t mux_prepare_for_read(TARGET_IO_t * This, UInt8_t * data, Size_t count, TARGET_DataId_t * id, UInt16_t * size, Bool_t * crcOk)
{
  symMultiplexCfg_t cfgMux;
  cfgMux.Buf.P = data;
  cfgMux.Buf.Size = count;
  cfgMux.symSize = 8;
  init_symMultiplex(&This->rxMux, &cfgMux);
  symMultiplex_setCount(&This->rxMux, count);

  if (!IO_Mux_prepare_for_read(&This->rxMux, size, crcOk)) return target_io_FAIL;

  if (!sym_multiplex_read_uint8(&This->rxMux, id, 8)) return target_io_FAIL;
  return target_io_OK;
}

static inline Bool_t read_sample(TARGET_IO_t * This, Sample_t * x)
{
#if SDR_SAMPLE_SIZE == 32
  return sym_multiplex_read_uint32(&This->rxMux, (UInt32_t*)x, 32);
#else
#error "TARGET_IO: UInt32_to_SAMPLE need implement"
#endif
}

static inline UInt16_t read_str(TARGET_IO_t * This, char * buf, Size_t size)
{
  buf[0] = '\0';
  UInt16_t len;
  if (!sym_multiplex_read_uint16(&This->rxMux, &len, 16)) return false;
  SDR_ASSERT(len < SDR_TARGET_IO_MUX_BUFSIZE);
  if (sym_multiplex_read_str(&This->rxMux, This->tmpBuf, len) != len) return false;
  buf[len] = 0;
  return len;
}

TARGET_IO_Result_t TARGET_IO_raw_rx_buf_handler(TARGET_IO_t * This, UInt8_t * data, Size_t count)
{
  UInt16_t pduSize = 0;
  Bool_t crcOk = true;
  HOST_DataId_t id;

  TARGET_IO_Result_t res;
  res = mux_prepare_for_read(This, data, count, &id, &pduSize, &crcOk);
  if (res != target_io_OK)
    return res;

  if (!crcOk)
    return target_io_FAIL_CRC;

  switch (id)
  {
  case TARGET_ResponseId:{
    UInt8_t resp;
    if (!sym_multiplex_read_uint8(&This->rxMux, &resp, 8)) return target_io_FAIL;
    switch(resp)
    {
    case TARGET_respOK:
    case TARGET_respFAIL:
      break;

    case TARGET_systemStarted:
      if (This->cfg.on_system_started)
        This->cfg.on_system_started(This->cfg.Master);
      break;

    case TARGET_respSTATE:{
      UInt8_t state;
      if (!sym_multiplex_read_uint8(&This->rxMux, &state, 8)) return target_io_FAIL;
      if (This->cfg.on_state_changed)
        This->cfg.on_state_changed(This->cfg.Master, state);
      break;}
    case TARGET_Params:{
      TARGET_Params_t params;
      if (!sym_multiplex_read_uint32(&This->rxMux, &params.timeout_ms, 32)) return target_io_FAIL;
      if (This->cfg.on_params_changed)
        This->cfg.on_params_changed(This->cfg.Master, &params);
      break;}
    case TARGET_NAME:
      if (!read_str(This, This->tmpBuf, SDR_TARGET_IO_MUX_BUFSIZE)) return target_io_FAIL;
      if (This->cfg.on_name_changed)
        This->cfg.on_name_changed(This->cfg.Master,This->tmpBuf);
      break;
    case TARGET_controlFORMAT:
    case TARGET_statusFORMAT:
    case TARGET_plotsFORMAT:{
      if (!read_str(This, This->tmpBuf, SDR_TARGET_IO_MUX_BUFSIZE)) return target_io_FAIL;
      if ((resp == TARGET_controlFORMAT) && This->cfg.on_control_format_changed)
        This->cfg.on_control_format_changed(This->cfg.Master, This->tmpBuf);
      else if ((resp == TARGET_statusFORMAT) && This->cfg.on_status_format_changed)
        This->cfg.on_status_format_changed(This->cfg.Master, This->tmpBuf);
      else if ((resp == TARGET_plotsFORMAT) && This->cfg.on_plots_format_changed)
        This->cfg.on_plots_format_changed(This->cfg.Master, This->tmpBuf);
      break;}
    case TARGET_LogMsg:
      if (!read_str(This, This->tmpBuf, SDR_TARGET_IO_MUX_BUFSIZE)) return target_io_FAIL;
      if (This->cfg.on_log_message)
        This->cfg.on_log_message(This->cfg.Master, This->tmpBuf);
      break;
    }
    break;}

  case TARGET_ElementCommandId:{
    UInt8_t cmd,elType,elId;
    if (!sym_multiplex_read_uint8(&This->rxMux, &cmd, 8)) return target_io_FAIL;
    if (!sym_multiplex_read_uint8(&This->rxMux, &elType, 8)) return target_io_FAIL;
    if (!sym_multiplex_read_uint8(&This->rxMux, &elId, 8)) return target_io_FAIL;
    if (!read_str(This, This->tmpBuf, SDR_TARGET_IO_MUX_BUFSIZE)) return target_io_FAIL;
    switch(cmd)
    {
    case TARGET_elemCmd_setValue:
      if (This->cfg.on_element_value_changed)
        This->cfg.on_element_value_changed(This->cfg.Master, elType, elId, This->tmpBuf);
      break;
    case TARGET_elemCmd_setParams:
      if (This->cfg.on_element_params_changed)
        This->cfg.on_element_params_changed(This->cfg.Master, elType, elId, This->tmpBuf);
      break;
    }
    break;}

  case TARGET_PlotCommandId:{
    UInt8_t plotId;
    if (!sym_multiplex_read_uint8(&This->rxMux, &plotId, 8)) return target_io_FAIL;
    UInt8_t cmd;
    if (!sym_multiplex_read_uint8(&This->rxMux, &cmd, 8)) return target_io_FAIL;
    read_str(This, This->tmpBuf, SDR_TARGET_IO_MUX_BUFSIZE); // не нужна проверка, т.к. поле может отсутствовать
    if (This->cfg.on_plot_command_ready)
      This->cfg.on_plot_command_ready(This->cfg.Master, plotId, (TARGET_PlotCommand_t)cmd, This->tmpBuf);
    break;}

  case TARGET_iqSamplesDataId:
  case TARGET_SamplesDataId:{
    UInt32_t Count;
    TARGET_DataId_t dataId;
    if (!sym_multiplex_read_uint8(&This->rxMux, &dataId, 8)) return target_io_FAIL;
    if (!sym_multiplex_read_uint32(&This->rxMux, &Count, 32)) return target_io_FAIL;
    if (id == TARGET_SamplesDataId)
    {
      SDR_ASSERT(Count < SDR_TARGET_IO_MUX_BUFSIZE/4);
      Sample_t * Buf = (Sample_t*)This->tmpBuf;
      UInt32_t i;
      for (i = 0; i < Count; ++i)
        read_sample(This, &Buf[i]);
      if (i != Count) break;
      if(This->cfg.on_samples_ready)
        This->cfg.on_samples_ready(This->cfg.Master, dataId, Buf, Count);
    }
    else
    {
      SDR_ASSERT(Count < SDR_TARGET_IO_MUX_BUFSIZE/(2*4));
      iqSample_t * Buf = (iqSample_t*)This->tmpBuf;
      UInt32_t i;
      for (i = 0; i < Count; ++i)
      {
        if (!read_sample(This, &Buf[i].i)) return target_io_FAIL;
        if (!read_sample(This, &Buf[i].q)) return target_io_FAIL;
      }
      if (i != Count) return target_io_FAIL;
      if(This->cfg.on_iq_samples_ready)
        This->cfg.on_iq_samples_ready(This->cfg.Master, dataId, Buf, Count);
    }
    break;}

  default:
    if (This->cbk.rx_UserData_ready)
      This->cbk.rx_UserData_ready(This->Parent, id, data+SDR_IO_MUX_HEADER_SIZE+1, pduSize-SDR_IO_MUX_HEADER_SIZE-1);
    break;
  }
  return target_io_OK;
}
