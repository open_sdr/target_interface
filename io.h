/*
 * io.h
 *
 *  Created on: 29 янв. 2018 г.
 *      Author: svetozar
 */

#ifndef SDR_TARGET_INTERFACE_IO_H_
#define SDR_TARGET_INTERFACE_IO_H_

#include <SDR/BASE/common.h>
#include <SDR/BASE/Abstract.h>
#include <SDR/BASE/Multiplex/symMultiplex.h>

#include "base/host.h"
#include "base/target.h"

#ifndef SDR_TARGET_IO_MUX_BUFSIZE
#define SDR_TARGET_IO_MUX_BUFSIZE 32768
#endif

#ifdef __cplusplus
extern "C"{
#endif

typedef void (*TARGET_system_started_fxn_t)(void * Master);
typedef void (*TARGET_params_changed_fxn_t)(void * This, TARGET_Params_t * Params);
typedef void (*TARGET_state_changed_fxn_t)(void * Master, TARGET_State_t state);
typedef void (*TARGET_name_changed_fxn_t)(void * Master, const char * name);
typedef void (*TARGET_format_changed_fxn_t)(void * Master, const char * format);
typedef void (*TARGET_element_value_changed_fxn_t)(void * Master, TARGET_ElementType_t type, TARGET_DataId_t id, const char * value);
typedef void (*TARGET_element_params_changed_fxn_t)(void * Master, TARGET_ElementType_t type, TARGET_DataId_t id, const char * params);
typedef void (*TARGET_plot_command_ready_fxn_t)(void * Master, TARGET_DataId_t id, TARGET_PlotCommand_t cmd, const char * ext);
typedef void (*TARGET_samples_ready_fxn_t)(void * Master, TARGET_DataId_t id, Sample_t * buf, Size_t count);
typedef void (*TARGET_iq_samples_ready_fxn_t)(void * Master, TARGET_DataId_t id, iqSample_t * buf, Size_t count);
typedef void (*TARGET_log_message_fxn_t)(void * Master, const char * message);

typedef struct
{
  void * Master;
  TARGET_system_started_fxn_t on_system_started;
  TARGET_params_changed_fxn_t on_params_changed;
  TARGET_state_changed_fxn_t on_state_changed;
  TARGET_name_changed_fxn_t on_name_changed;
  TARGET_format_changed_fxn_t on_control_format_changed;
  TARGET_format_changed_fxn_t on_status_format_changed;
  TARGET_format_changed_fxn_t on_plots_format_changed;
  TARGET_element_value_changed_fxn_t on_element_value_changed;
  TARGET_element_params_changed_fxn_t on_element_params_changed;
  TARGET_plot_command_ready_fxn_t on_plot_command_ready;
  TARGET_samples_ready_fxn_t on_samples_ready;
  TARGET_iq_samples_ready_fxn_t on_iq_samples_ready;
  TARGET_log_message_fxn_t on_log_message;
} TARGET_IO_Cfg_t;

typedef void (*TARGET_IO_rx_UserData_ready_t)(void * This, TARGET_DataId_t id, UInt8_t * data, Size_t count);
typedef struct
{
  TARGET_IO_rx_UserData_ready_t rx_UserData_ready;
} TARGET_IO_Callback_t;

typedef struct
{
  TARGET_IO_Cfg_t cfg;

  void * Parent;
  TARGET_IO_Callback_t cbk;

  symMultiplex_t txMux, rxMux;
  UInt8_t txMuxBuf[SDR_TARGET_IO_MUX_BUFSIZE];
  UInt8_t tmpBuf[SDR_TARGET_IO_MUX_BUFSIZE];
} TARGET_IO_t;

void init_TARGET_IO(TARGET_IO_t * This, TARGET_IO_Cfg_t * Cfg);

#define on_CBK(name) do{if(This->cfg.on_##name)This->cfg.on_##name(This->cfg.Master);}while(0)

INLINE void TARGET_IO_setCallback(TARGET_IO_t * This, void * Parent, TARGET_IO_Callback_t * cbk)
{
  This->Parent = Parent;
  This->cbk = *cbk;
}

Bool_t TARGET_IO_send_data(TARGET_IO_t * This, HOST_DataId_t id, UInt8_t * Buf, Size_t Count);

Bool_t TARGET_IO_send_params(TARGET_IO_t * This, TARGET_Params_t * Params);

Bool_t TARGET_IO_send_command(TARGET_IO_t * This, HOST_Command_t cmd);

Bool_t TARGET_IO_send_value(TARGET_IO_t * This, TARGET_DataId_t id, const char * value);

#ifdef __cplusplus
}
#endif

#undef on_CBK

#endif /* SDR_TARGET_INTERFACE_IO_H_ */
