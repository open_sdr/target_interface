/*
 * sdr_host_interface_io.h
 *
 *  Created on: 19 янв. 2018 г.
 *      Author: svetozar
 */

#ifndef SDR_TARGET_INTERFACE_INTERNAL_IO_H_
#define SDR_TARGET_INTERFACE_INTERNAL_IO_H_

#include "../io.h"

#ifdef __cplusplus
extern "C"{
#endif

extern Size_t TARGET_IO_raw_send_buf(TARGET_IO_t * This, UInt8_t * Buf, Size_t Count);

typedef enum
{
  target_io_OK = 0,

  target_io_FAIL = -0x10,
  target_io_FAIL_CRC = -0x11
} TARGET_IO_Result_t;

TARGET_IO_Result_t TARGET_IO_raw_rx_buf_handler(TARGET_IO_t * This, UInt8_t * data, Size_t count);

#ifdef __cplusplus
}
#endif

#endif /* SDR_TARGET_INTERFACE_INTERNAL_IO_H_ */
